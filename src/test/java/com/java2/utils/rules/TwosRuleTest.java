package com.java2.utils.rules;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TwosRuleTest {
    TwosRule rule = new TwosRule();

    private static Stream<Arguments> provideYahtzeeDraws() {
        return Stream.of(
                Arguments.of(new int[]{2,2,2,2,2}, 10),
                Arguments.of(new int[]{5,2,2,2,2}, 8),
                Arguments.of(new int[]{1,2,2,2,1}, 6),
                Arguments.of(new int[]{2,4,2,1,5}, 4),
                Arguments.of(new int[]{1,1,3,2,6}, 2),
                Arguments.of(new int[]{6,3,6,4,6}, 0)
        );
    }

    @ParameterizedTest
    @MethodSource("provideYahtzeeDraws")
    void computeTest(int[] list, int result) {
        assertEquals(result, rule.compute(list));
    }
}