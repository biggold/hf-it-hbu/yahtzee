package com.java2.utils.rules;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class OnesRuleTest {
    OnesRule rule = new OnesRule();

    private static Stream<Arguments> provideYahtzeeDraws() {
        return Stream.of(
                Arguments.of(new int[]{1,2,3,4,5}, 1),
                Arguments.of(new int[]{5,4,2,6,5}, 0),
                Arguments.of(new int[]{1,2,3,4,1}, 2),
                Arguments.of(new int[]{5,4,2,1,5}, 1),
                Arguments.of(new int[]{1,2,1,4,6}, 2),
                Arguments.of(new int[]{6,2,6,4,6}, 0),
                Arguments.of(new int[]{1,2,1,4,1}, 3)
        );
    }

    @ParameterizedTest
    @MethodSource("provideYahtzeeDraws")
    void computeTest(int[] list, int result) {
        assertEquals(result, rule.compute(list));
    }
}