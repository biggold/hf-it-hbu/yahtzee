package com.java2.utils.rules;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ThreesRuleTest {
    ThreesRule rule = new ThreesRule();

    private static Stream<Arguments> provideYahtzeeDraws() {
        return Stream.of(
                Arguments.of(new int[]{3,3,3,3,3}, 15),
                Arguments.of(new int[]{5,3,3,3,3}, 12),
                Arguments.of(new int[]{1,3,3,4,3}, 9),
                Arguments.of(new int[]{5,4,2,3,3}, 6),
                Arguments.of(new int[]{1,2,1,4,3}, 3),
                Arguments.of(new int[]{1,2,1,4,1}, 0)
        );
    }

    @ParameterizedTest
    @MethodSource("provideYahtzeeDraws")
    void computeTest(int[] list, int result) {
        assertEquals(result, rule.compute(list));
    }
}