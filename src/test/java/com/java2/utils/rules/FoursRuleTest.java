package com.java2.utils.rules;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FoursRuleTest {
    FoursRule rule = new FoursRule();

    private static Stream<Arguments> provideYahtzeeDraws() {
        return Stream.of(
                Arguments.of(new int[]{4,4,4,4,4}, 20),
                Arguments.of(new int[]{5,4,4,4,4}, 16),
                Arguments.of(new int[]{1,4,3,4,4}, 12),
                Arguments.of(new int[]{5,4,2,1,4}, 8),
                Arguments.of(new int[]{1,2,1,4,6}, 4),
                Arguments.of(new int[]{1,2,1,3,1}, 0)
        );
    }

    @ParameterizedTest
    @MethodSource("provideYahtzeeDraws")
    void computeTest(int[] list, int result) {
        assertEquals(result, rule.compute(list));
    }
}