package com.java2.utils.rules;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FourOfAKindRuleTest {
    FourOfAKindRule rule = new FourOfAKindRule();

    private static Stream<Arguments> provideYahtzeeDraws() {
        return Stream.of(
                Arguments.of(new int[]{3,3,3,3,1}, 13),
                Arguments.of(new int[]{5,2,2,2,2}, 13),
                Arguments.of(new int[]{1,6,6,6,6}, 25),
                Arguments.of(new int[]{2,5,5,5,5}, 22),
                Arguments.of(new int[]{6,6,6,6,6}, 30),
                Arguments.of(new int[]{6,3,6,4,2}, 0)
        );
    }

    @ParameterizedTest
    @MethodSource("provideYahtzeeDraws")
    void computeTest(int[] list, int result) {
        assertEquals(result, rule.compute(list));
    }
}