package com.java2.utils.rules;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FivesRuleTest {
    FivesRule rule = new FivesRule();

    private static Stream<Arguments> provideYahtzeeDraws() {
        return Stream.of(
                Arguments.of(new int[]{5,5,5,5,5}, 25),
                Arguments.of(new int[]{5,4,5,5,5}, 20),
                Arguments.of(new int[]{1,2,5,5,5}, 15),
                Arguments.of(new int[]{3,4,2,5,5}, 10),
                Arguments.of(new int[]{1,2,1,4,5}, 5),
                Arguments.of(new int[]{1,2,1,4,1}, 0)
        );
    }

    @ParameterizedTest
    @MethodSource("provideYahtzeeDraws")
    void computeTest(int[] list, int result) {
        assertEquals(result, rule.compute(list));
    }
}