package com.java2.utils.rules;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SixesRuleTest {
    SixesRule rule = new SixesRule();

    private static Stream<Arguments> provideYahtzeeDraws() {
        return Stream.of(
                Arguments.of(new int[]{6,6,6,6,6}, 30),
                Arguments.of(new int[]{5,6,6,6,6}, 24),
                Arguments.of(new int[]{1,2,6,6,6}, 18),
                Arguments.of(new int[]{5,4,2,6,6}, 12),
                Arguments.of(new int[]{1,2,1,4,6}, 6),
                Arguments.of(new int[]{1,2,1,4,1}, 0)
        );
    }

    @ParameterizedTest
    @MethodSource("provideYahtzeeDraws")
    void computeTest(int[] list, int result) {
        assertEquals(result, rule.compute(list));
    }
}