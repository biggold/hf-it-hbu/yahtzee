package com.java2.utils.rules;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SmallStraightRuleTest {
    SmallStraightRule rule = new SmallStraightRule();

    private static Stream<Arguments> provideYahtzeeDraws() {
        return Stream.of(
                Arguments.of(new int[]{1,2,3,4,6}, 30),
                Arguments.of(new int[]{2,3,4,5,6}, 30),
                Arguments.of(new int[]{1,2,2,3,4}, 30),
                Arguments.of(new int[]{1,3,4,5,6}, 30),
                Arguments.of(new int[]{1,1,3,2,4}, 30),
                Arguments.of(new int[]{6,3,6,4,6}, 0)
        );
    }

    @ParameterizedTest
    @MethodSource("provideYahtzeeDraws")
    void computeTest(int[] list, int result) {
        assertEquals(result, rule.compute(list));
    }
}