package com.java2.utils.rules;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ThreeOfAKindRuleTest {
    ThreeOfAKindRule rule = new ThreeOfAKindRule();

    private static Stream<Arguments> provideYahtzeeDraws() {
        return Stream.of(
                Arguments.of(new int[]{3,3,3,1,1}, 11),
                Arguments.of(new int[]{5,2,2,2,2}, 13),
                Arguments.of(new int[]{1,2,6,6,6}, 21),
                Arguments.of(new int[]{2,4,5,5,5}, 21),
                Arguments.of(new int[]{6,6,6,6,6}, 30),
                Arguments.of(new int[]{6,3,6,4,2}, 0)
        );
    }

    @ParameterizedTest
    @MethodSource("provideYahtzeeDraws")
    void computeTest(int[] list, int result) {
        assertEquals(result, rule.compute(list));
    }
}