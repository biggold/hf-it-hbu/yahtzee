package com.java2.utils.rules;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class LargeStraightRuleTest {
    LargeStraightRule rule = new LargeStraightRule();

    private static Stream<Arguments> provideYahtzeeDraws() {
        return Stream.of(
                Arguments.of(new int[]{1,2,3,4,5}, 40),
                Arguments.of(new int[]{2,3,4,5,6}, 40),
                Arguments.of(new int[]{3,2,4,5,6}, 40),
                Arguments.of(new int[]{5,3,2,4,1}, 40),
                Arguments.of(new int[]{1,1,3,2,6}, 0),
                Arguments.of(new int[]{6,3,6,4,6}, 0)
        );
    }

    @ParameterizedTest
    @MethodSource("provideYahtzeeDraws")
    void computeTest(int[] list, int result) {
        assertEquals(result, rule.compute(list));
    }
}