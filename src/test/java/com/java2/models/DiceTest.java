package com.java2.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DiceTest {
    Dice dice;

    @BeforeEach
    void initDice() {
        dice = new Dice();
    }

    @Test
    void getValueNotRolledTest() {
        assertEquals(0, dice.getValue());
    }

    /**
     * We repeat this test multiple times to make sure the
     * random generator in roll is generating numbers from 1-6!
     * <p>
     * With the high number of repeats we try to eliminate the possibility of wrong values!
     */
    @RepeatedTest(2000)
    void getValueRolledTest() {
        dice.roll();
        // Value should be less or equal to 6
        assertTrue(6 >= dice.getValue());
        // Value should be greater or equal to 1
        assertTrue(1 <= dice.getValue());
    }

    // We run this test multiple times to reduce the risk of rolling two times the same result.
    @RepeatedTest(200)
    void setFreezeTest() {
        dice.roll();
        int value = dice.getValue();
        // We should have a number here!
        assertNotEquals(0, value);
        dice.setFreeze(true);
        // Roll again to see if the value changes
        dice.roll();
        // It should be the same because the dice isn't allowed to roll.
        assertEquals(value, dice.getValue());
    }

    @Test
    void unsetFreezeTest() {
        // Freeze the dice from the beginning
        dice.setFreeze(true);
        // Roll to see if the value changes
        dice.roll();
        // Shouldn't have changed
        assertEquals(0, dice.getValue());
        // Unfreeze and roll the dice
        dice.setFreeze(false);
        dice.roll();
        // We should no have a value here
        assertNotEquals(0, dice.getValue());
    }

    @Test
    void resetTest() {
        dice.roll();
        // Should now be a number != 0
        assertNotEquals(0, dice.getValue());
        dice.reset();
        // Value should now be 0 again
        assertEquals(0, dice.getValue());
    }

    // We run this test multiple times to reduce the risk of rolling twice the same
    @RepeatedTest(200)
    void resetFrozenTest() {
        dice.roll();
        // Should now be a number != 0
        int value = dice.getValue();
        assertNotEquals(0, value);
        // Freeze the dice
        dice.setFreeze(true);
        // Validate the dice is frozen
        dice.roll();
        assertEquals(value, dice.getValue());
        // Reset an check the value
        dice.reset();
        assertEquals(0, dice.getValue());
        // Check if we can roll again
        dice.roll();
        assertNotEquals(0, value);
    }
}