package com.java2.models;

import com.java2.utils.rules.AbstractRule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ScoreEntryTest {
    ScoreEntry entry;
    class TestRule extends AbstractRule {
        public int compute(int[] ist) { return 1;}
    }

    @Test
    void getValueTest() {
        entry = new ScoreEntry(new TestRule(), true);
        assertEquals(0, entry.getValue());
    }

    @Test
    void isUpperCardTest() {
        entry = new ScoreEntry(new TestRule(), true);
        assertTrue(entry.isUpperCard());
    }

    @Test
    void isNotUpperCardTest() {
        entry = new ScoreEntry(new TestRule(), false);
        assertFalse(entry.isUpperCard());
    }

    @Test
    void getValueString() {
        entry = new ScoreEntry(new TestRule(), false);
        assertEquals("0", entry.getValueString());
    }

    @Test
    void setValue() {
        entry = new ScoreEntry(new TestRule(), false);
        entry.setValue(4);
        assertEquals(4, entry.getValue());
        assertEquals("4", entry.getValueString());
    }

    @Test
    void compute() {
        entry = new ScoreEntry(new TestRule(), false);
        assertEquals(1,entry.compute(new int[]{}));
    }
}