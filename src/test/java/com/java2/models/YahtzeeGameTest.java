package com.java2.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class YahtzeeGameTest {
    YahtzeeGame game;
    @BeforeEach
    void initYahtzeeGame() {
        game = new YahtzeeGame();
    }

    @Test
    void addRoundTest() {
        // We simulate 13 rounds, for 12 we should receive true and for the 13 one we should receive false
        for(int i=1; i<=12; i++) {
            assertTrue(YahtzeeGame.addRound());
        }
        assertFalse(YahtzeeGame.addRound());
    }

    @Test
    void rollCounterTest() {
        assertEquals(0, YahtzeeGame.getRollCounter());
        YahtzeeGame.addRoll();
        assertEquals(1, YahtzeeGame.getRollCounter());
        YahtzeeGame.addRoll();
        assertEquals(2, YahtzeeGame.getRollCounter());
    }

    @Test
    void getDiceValuesTest() {
        assertArrayEquals(new int[]{0,0,0,0,0}, YahtzeeGame.getDiceValues());
    }

    @Test
    void getDicesTest() {
        assertEquals(5, YahtzeeGame.getDices().length);
        for(Dice dice: YahtzeeGame.getDices()) {
            assertEquals(Dice.class, dice.getClass());
        }
    }

    @Test
    void getScoreCardTest() {
        assertEquals(ScoreCard.class, YahtzeeGame.getScoreCard().getClass());
    }

}