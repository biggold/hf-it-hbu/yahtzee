package com.java2.models;

import com.java2.utils.rules.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class ScoreCardTest {
    private ScoreCard card;
    @BeforeEach
    void initScoreCard() {
        card = new ScoreCard();
    }

    @Test
    void getScoreFieldsTest() {
        // We check if the scorecard contains the expected amount of items -> its a final variable
        assertEquals(13, card.getScoreFields().size());
    }


    private static Stream<Arguments> provideScoreCards() {
        // Hashmap<Score Entry name, Score Value>, bonus, upperTotal, lowerTotal, total
        return Stream.of(
                Arguments.of(new HashMap<String, Integer>() {{
                    put("Ones", 5);
                    put("Twos", 10);
                    put("Threes", 15);
                    put("Fours", 20);
                    put("Fives", 25);
                    put("Sixes", 30);
                    put("3 of a Kind", 20);
                    put("4 of a Kind", 20);
                    put("Small Straight", 20);
                    put("Large Straight", 30);
                    put("Full House", 40);
                    put("Chance", 20);
                    put("Yahtzee", 50);
                }}, 35, 105, 200, 340),
                Arguments.of(new HashMap<String, Integer>() {{
                    put("Ones", 2);
                    put("Twos", 4);
                    put("Threes", 10);
                    put("Fours", 0);
                    put("Fives", 5);
                    put("Sixes", 6);
                    put("3 of a Kind", 18);
                    put("4 of a Kind", 20);
                    put("Small Straight", 0);
                    put("Large Straight", 30);
                    put("Full House", 40);
                    put("Chance", 12);
                    put("Yahtzee", 0);
                }}, 0, 27, 120, 147)
        );
    }

    /**
     * Here we see if the bonus, total, upper/lower total works including the calculation
     */
    @ParameterizedTest
    @MethodSource("provideScoreCards")
    void calculate(HashMap<String, Integer> scoreFields, int bonus, int upperTotal, int lowerTotal, int total) {
        // Add values to score card
        for (Map.Entry<String, Integer> entry : scoreFields.entrySet()) {
            card.getScoreFields().get(entry.getKey()).setValue(entry.getValue());
        }
        // Lets calculate
        card.calculate();
        assertEquals(bonus, card.getBonus());
        assertEquals(upperTotal, card.getUpperTotal());
        assertEquals(lowerTotal, card.getLowerTotal());
        assertEquals(total, card.getTotal());
    }
}