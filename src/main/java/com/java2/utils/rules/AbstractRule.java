package com.java2.utils.rules;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * This class describes how a game rule should look like with some basic helper functions in it.
 */
public abstract class AbstractRule {
    /**
     * Game Score Rule
     *
     * @param list a list object with the dices like new int[]{1,2,3,4,5} describing the dices
     * @return The computet result as INT
     */
    public abstract int compute(int[] list);

    /**
     * Counts the occurrence we see a dice's eyes
     *
     * @param list A list with dice numbers
     * @return Key is the dice (eyes), Value is how many of those we saw
     */
    protected static HashMap<Integer, Integer> countFrequencies(int[] list) {
        HashMap<Integer, Integer> counter = new HashMap<Integer, Integer>();
        for (Integer i : list) {
            if (counter.containsKey(i)) {
                counter.put(i, counter.get(i) + 1);
            } else {
                counter.put(i, 1);
            }
        }
        return counter;
    }

    /**
     * Counts the occurrence of a specified eye
     *
     * @param list          a list object with the dices like new int[]{1,2,3,4,5} describing the dices
     * @param countedNumber Number to check for occurrence
     * @return Returns the occurrence amount
     */
    protected int computeSingles(int[] list, int countedNumber) {
        int result = 0;
        for (Integer i : list) {
            if (countedNumber == i) {
                result = result + i;
            }
        }
        return result;
    }

    /**
     * Validates a dice draw against multiple combinations to check if it matches. Normally used for straights
     *
     * @param list      a list object with the dices like new int[]{1,2,3,4,5} describing the dices
     * @param straights list of possible straigts
     * @return Returns if it matched or not
     */
    protected boolean isStraight(int[] list, int[][] straights) {
        int end = list.length;
        Set<Integer> set = new HashSet<Integer>();
        // Remove duplicate keys
        for (int j : list) {
            set.add(j);
        }
        //Creating an empty integer array
        int[] tmpList = new int[set.size()];
        //Converting Set object to integer array
        int j = 0;
        for (Integer i : set) {
            tmpList[j++] = i;
        }
        Arrays.sort(tmpList);
        for (int[] straight : straights) {
            if (Arrays.equals(tmpList, straight)) {
                return true;
            }
        }
        return false;
    }
}
