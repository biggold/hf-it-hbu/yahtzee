package com.java2.utils.rules;

public class FoursRule extends AbstractRule {
    /**
     * Game Score Rule
     *
     * @param list a list object with the dices like new int[]{1,2,3,4,5} describing the dices
     * @return The computet result as INT
     */
    @Override
    public int compute(int[] list) {
        return computeSingles(list ,4);
    }
}
