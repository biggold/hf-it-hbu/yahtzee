package com.java2.utils.rules;

import java.util.HashMap;
import java.util.Map;


/**
 * Implements the rule for the FullHouseRule field
 */
public class FullHouseRule extends AbstractRule {
    /**
     * Game Score Rule
     *
     * @param list a list object with the dices like new int[]{1,2,3,4,5} describing the dices
     * @return The computet result as INT
     */
    public int compute(int[] list) {
        HashMap<Integer, Integer> map = countFrequencies(list);
        boolean three = false;
        boolean two = false;
        for (Map.Entry<Integer, Integer> element : map.entrySet()) {
            if (element.getValue() == 3) {
                // Three equal dices
                three = true;
            } else if (element.getValue() == 2) {
                // Two equal dices
                two = true;
            }
        }
        if (two && three) {
            return 25;
        } else {
            return 0;
        }
    }
}
