package com.java2.utils.rules;

/**
 * Implements the rule for the SmallStraightRule field
 */
public class SmallStraightRule extends AbstractRule {
    /**
     * Game Score Rule
     *
     * @param list a list object with the dices like new int[]{1,2,3,4,5} describing the dices
     * @return The computet result as INT
     */
    private final int[][] straights = {{1, 2, 3, 4}, {2, 3, 4, 5}, {3, 4, 5, 6}, {1, 2, 3, 4, 5}, {2, 3, 4, 5, 6}, {1, 2, 3, 4, 6}, {1, 3, 4, 5, 6}};

    @Override
    public int compute(int[] list) {
        if (isStraight(list, straights)) {
            return 30;
        } else {
            return 0;
        }
    }
}
