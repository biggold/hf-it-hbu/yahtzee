package com.java2.utils.rules;

import java.util.HashMap;
import java.util.Map;

public class ChanceRule extends AbstractRule {
    /**
     * Game Score Rule
     *
     * @param list a list object with the dices like new int[]{1,2,3,4,5} describing the dices
     * @return The computet result as INT
     */
    public int compute(int[] list) {
        int result = 0;
        for (Integer i : list) {
            result = result + i;
        }
        return result;
    }
}
