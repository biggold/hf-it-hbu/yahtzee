package com.java2.controllers;

import com.java2.models.YahtzeeGame;
import com.java2.ui.views.DicePanel;
import com.java2.ui.views.ScoreCardPanel;
import com.java2.ui.views.ScorePanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * The OptionsListener is responsible for the event handling of the
 * Options buttons namely "New Game".
 *
 * @author Cyrill Näf
 */
public class OptionsListener implements ActionListener {
    /**
     * Usualy a New Game button press
     *
     * @param e A button press action event.
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String actionCommand = e.getActionCommand();
        if (actionCommand.equals("New Game")) {
            // Reset everything
            YahtzeeGame.reset();
            ScoreCardPanel.reset();
            ScorePanel.reset();
            DicePanel.reset();
        }
    }
}
