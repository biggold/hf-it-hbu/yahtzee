package com.java2.controllers;

import com.java2.models.Dice;
import com.java2.models.YahtzeeGame;
import com.java2.ui.views.DicePanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JToggleButton;

/**
 * The RollTheDiceListener is responsible for the event handling of the
 * Dices-ToggleButtons and the Roll Button.
 *
 * @author Cyrill Näf
 */
public class RollTheDiceListener implements ActionListener {
    /**
     * Controlls the Dices and the Roll button
     *
     * @param e A button press action event.
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String actionCommand = e.getActionCommand();
        if (actionCommand.equals("Roll")) {
            if (YahtzeeGame.getRollCounter() < 3) {
                for (Dice dice : YahtzeeGame.getDices()) {
                    dice.roll();
                }
                DicePanel.setDiceImages(YahtzeeGame.getDiceValues());
                YahtzeeGame.addRoll();
            }
            // We should disable the roll button here
            if (YahtzeeGame.getRollCounter() >= 3) {
                DicePanel.getRollButton().setEnabled(false);
            }
        } else if (actionCommand.matches("[01234]")) {
            JToggleButton button = (JToggleButton) e.getSource();
            YahtzeeGame.getDices()[Integer.parseInt(e.getActionCommand())].setFreeze(button.isSelected());
        }
    }
}
