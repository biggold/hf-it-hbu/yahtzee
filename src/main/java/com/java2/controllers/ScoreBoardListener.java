package com.java2.controllers;

import com.java2.models.ScoreCard;
import com.java2.models.ScoreEntry;
import com.java2.models.YahtzeeGame;
import com.java2.ui.views.DicePanel;
import com.java2.ui.views.ScoreCardPanel;
import com.java2.ui.views.ScorePanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * The ScoreBoardListener is responsible for the event handling of the
 * ScoreBoard buttons.
 *
 * @author Cyrill Näf
 */
public class ScoreBoardListener implements ActionListener {
    /**
     * ScorePanel button listener
     *
     * @param e A button press action event.
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (YahtzeeGame.getRollCounter() > 0) {
            String actionCommand = e.getActionCommand();
            // Update the score panel -> Some magic ;)
            ScoreEntry scoreCardField = YahtzeeGame.getScoreCard().getScoreFields().get(actionCommand);
            scoreCardField.setValue(scoreCardField.compute(YahtzeeGame.getDiceValues()));
            ScoreCardPanel.scoreFields.get(actionCommand).getButton().setEnabled(false);
            ScoreCardPanel.scoreFields.get(actionCommand).getTextField().setText(scoreCardField.getValueString());
            DicePanel.getRollButton().setEnabled(YahtzeeGame.addRound());
            // Update Score board and so on
            DicePanel.setInitialButtonIcon();

            ScoreCard scoreCard = YahtzeeGame.getScoreCard();
            ScorePanel.getTextField().setText(String.valueOf(scoreCard.getTotal()));
            ScoreCardPanel.scoreFields.get("Bonus").getTextField().setText(String.valueOf(scoreCard.getBonus()));
            ScoreCardPanel.scoreFields.get("Lower Total").getTextField().setText(String.valueOf(scoreCard.getLowerTotal()));
            ScoreCardPanel.scoreFields.get("Upper Total").getTextField().setText(String.valueOf(scoreCard.getUpperTotal()));
        }
    }
}
