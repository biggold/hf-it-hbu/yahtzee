package com.java2;

import com.java2.ui.views.MainWindow;

/**
 * Main class to lunch the application
 *
 * @author Cyrill Näf
 */
public final class main {
    public static void main(String[] args) {
        MainWindow mainWindow = new MainWindow();
    }
}
