package com.java2.models;

import com.java2.utils.rules.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Class handling the ScoreBoard (points/score) functionality
 *
 * @author Cyrill Näf
 */
public class ScoreCard {
    private final HashMap<String, ScoreEntry> scoreFields = new HashMap<>() {{
        put("Ones", new ScoreEntry(new OnesRule(), true));
        put("Twos", new ScoreEntry(new TwosRule(), true));
        put("Threes", new ScoreEntry(new ThreesRule(), true));
        put("Fours", new ScoreEntry(new FoursRule(), true));
        put("Fives", new ScoreEntry(new FivesRule(), true));
        put("Sixes", new ScoreEntry(new SixesRule(), true));
        put("3 of a Kind", new ScoreEntry(new ThreeOfAKindRule(), false));
        put("4 of a Kind", new ScoreEntry(new FourOfAKindRule(), false));
        put("Small Straight", new ScoreEntry(new SmallStraightRule(), false));
        put("Large Straight", new ScoreEntry(new LargeStraightRule(), false));
        put("Full House", new ScoreEntry(new FullHouseRule(), false));
        put("Chance", new ScoreEntry(new ChanceRule(), false));
        put("Yahtzee", new ScoreEntry(new YahtzeeRule(), false));
    }};

    private int bonus;
    private int upperTotal;
    private int lowerTotal; // May be a function instead of a variable

    /**
     * Returns the ScoreFields
     *
     * @return HashMap containing the scorefields <String = ScoreField Name, ScoreEntry = ScoreField object>
     */
    public HashMap<String, ScoreEntry> getScoreFields() {
        return scoreFields;
    }

    /**
     * Returns the bonus of the card
     *
     * @return Bonus in INT form
     */
    public int getBonus() {
        return bonus;
    }

    /**
     * Returns the total of the card
     *
     * @return Total in INT form
     */
    public int getTotal() {
        return bonus + upperTotal + lowerTotal;
    }

    /**
     * Returns the UpperTotal of the card
     *
     * @return UpperTotal in INT form
     */
    public int getUpperTotal() {
        return upperTotal;
    }

    /**
     * Returns the LowerTotal of the card
     *
     * @return LowerTotal in INT form
     */
    public int getLowerTotal() {
        return lowerTotal;
    }

    /**
     * Calculates the Score of the current game (Bonus, Upper-&LowerTotal, Total)
     */
    public void calculate() {
        upperTotal = 0;
        lowerTotal = 0;
        bonus = 0;
        for (Map.Entry<String, ScoreEntry> entry : scoreFields.entrySet()) {
            if (entry.getValue().isUpperCard()) {
                upperTotal = upperTotal + entry.getValue().getValue();
            } else {
                lowerTotal = lowerTotal + entry.getValue().getValue();
            }
        }
        if (upperTotal >= 63) {
            bonus = 35;
        }
    }
}
