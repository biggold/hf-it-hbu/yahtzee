package com.java2.models;


/**
 * Holds a game. Here we would have to start to add multiplayer setup by transforming the class from static to object based.
 *
 * @author Cyrill Näf
 */
public class YahtzeeGame {
    public static final int NUMBER_OF_DICES = 5;
    private static int roundCounter;
    private static ScoreCard scoreCard = new ScoreCard();
    private static final Dice[] dices = {new Dice(), new Dice(), new Dice(), new Dice(), new Dice()};
    private static int rollCounter;

    /**
     * Constructor
     */
    public YahtzeeGame() {
        reset();
    }

    /**
     * Returns the ScoreCard of the current game
     *
     * @return ScoreCard
     */
    public static ScoreCard getScoreCard() {
        return scoreCard;
    }

    /**
     * Returns the rollCounter
     *
     * @return rollCounter
     */
    public static int getRollCounter() {
        return rollCounter;
    }

    /**
     * Returns the dices
     *
     * @return returns our dice array
     */
    public static Dice[] getDices() {
        return dices;
    }

    /**
     * Returns the values of the drawed dices
     *
     * @return A list from all dices entry 0 = dice 1 and so on.
     */
    public static int[] getDiceValues() {
        int[] diceValues = new int[5];
        for (int i = 0; i < dices.length; i++) {
            diceValues[i] = dices[i].getValue();
        }
        return diceValues;
    }

    /**
     * add's a played round
     *
     * @return Returns true if the game can be continued and false if not.
     */
    public static boolean addRound() {
        roundCounter = roundCounter + 1;
        scoreCard.calculate();
        if (roundCounter <= 12) {
            resetRollCounter();
            return true;
        } else {
            return false;
        }
    }

    /**
     * Add's a draw/roll to the current round
     */
    public static void addRoll() {
        rollCounter = rollCounter + 1;
    }

    /**
     * Resets the RollCounter of the class
     */
    private static void resetRollCounter() {
        rollCounter = 0;
        // reset all freezed dices
        for (Dice dice : dices) {
            dice.setFreeze(false);
        }
    }

    /**
     * Reset's the game part of the class
     */
    private static void resetGame() {
        scoreCard = new ScoreCard();
        roundCounter = 0;
    }

    /**
     * Reset's the Yahtzee game to its initial/desired state
     */
    public static void reset() {
        resetGame();
        resetRollCounter();
    }

}

