package com.java2.models;

import com.java2.utils.rules.AbstractRule;

/**
 * Describes a ScoreBoard entry
 *
 * @author Cyrill Näf
 */
public class ScoreEntry {
    private int value;
    private boolean upperCard;
    private final AbstractRule rule;

    /**
     * Constructor
     * @param rule Rule is class AbstractRule implementation describing the game calculation rule of the field.
     * @param upperCard If it is located on the upper or lower card.
     */
    public ScoreEntry(AbstractRule rule, boolean upperCard) {
        this.rule = rule;
        this.upperCard = upperCard;
    }

    /**
     * Returns the score value of this entry
     * @return ScoreValue as INT
     */
    public int getValue() {
        return value;
    }

    /**
     * Is it in the upperCard?
     * @return UpperCard yes/no
     */
    public boolean isUpperCard() {
        return upperCard;
    }

    /**
     * Returns the value as string
     * @return Value as string
     */
    public String getValueString() {
        return String.valueOf(value);
    }

    /**
     * Sets the value of the score card
     * @param value Any integer
     */
    public void setValue(int value) {
        this.value = value;
    }

    /**
     * Calculates the game rule of the abstract class -> Wrapper
     * @param list a list object with the dices like new int[]{1,2,3,4,5} describing the dices
     * @return Result of the calculation as INT
     */
    public int compute(int[] list) {
        return rule.compute(list);
    }
}
