package com.java2.models;

import java.util.Random;

/**
 * This class describes how a Dice is constructed
 *
 * @author Cyrill Näf
 */
public class Dice {
    private boolean isFrozen;
    private int value;

    /**
     * Constructor
     */
    public Dice() {
        reset();
    }

    public void roll() {
        if (!isFrozen) {
            value = new Random().nextInt(6) + 1;
        }
    }

    /**
     * Gets the value of the dice
     *
     * @return Dice eyes in form of integer
     */
    public int getValue() {
        return value;
    }

    /**
     * Freeze/unfrezzes a dice -> Change it's roll behavior
     *
     * @param freeze Freeze/unfreeze toggle
     */
    public void setFreeze(boolean freeze) {
        isFrozen = freeze;
    }

    /**
     * Reset's the dice to it's initial state
     */
    public void reset() {
        isFrozen = false;
        value = 0;
    }
}
