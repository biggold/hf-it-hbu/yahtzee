package com.java2.ui.views;

import com.java2.ui.components.YPanel;

import javax.swing.*;
import java.awt.*;

/**
 * Class defining the ScorePanel
 *
 * @author Cyrill Näf
 */
public class ScorePanel extends YPanel {
    private static JTextField textField;

    /**
     * Constructor
     *
     * @param title The display name of the panel, passed to YPanel
     */
    public ScorePanel(String title) {
        super(title);
        setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.BOTH;
        gbc.weightx = gbc.weighty = 1;
        gbc.gridy = gbc.gridx = 0;
        textField = new JTextField();
        textField.setEditable(false);
        add(textField, gbc);
    }

    /**
     * Returns the TextField of the score panel
     *
     * @return JTextField
     */
    public static JTextField getTextField() {
        return textField;
    }

    /**
     * Resets the panel to it's desired state
     */
    public static void reset() {
        textField.setText(null);
    }
}
