package com.java2.ui.views;

import com.java2.ui.components.ScoreField;
import com.java2.ui.components.YPanel;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Class defining the panel for the ScoreCard
 *
 * @author Cyrill Näf
 */
public class ScoreCardPanel extends YPanel {
    public static final HashMap<String, ScoreField> scoreFields = new HashMap<>() {{
        put("Ones", new ScoreField("Ones", 0, 0));
        put("Twos", new ScoreField("Twos", 1, 0));
        put("Threes", new ScoreField("Threes", 2, 0));
        put("Fours", new ScoreField("Fours", 3, 0));
        put("Fives", new ScoreField("Fives", 4, 0));
        put("Sixes", new ScoreField("Sixes", 5, 0));
        put("Bonus", new ScoreField("Bonus", 6, 0));
        put("Upper Total", new ScoreField("Upper Total", 7, 0));
        put("3 of a Kind", new ScoreField("3 of a Kind", 0, 2));
        put("4 of a Kind", new ScoreField("4 of a Kind", 1, 2));
        put("Small Straight", new ScoreField("Small Straight", 2, 2));
        put("Large Straight", new ScoreField("Large Straight", 3, 2));
        put("Full House", new ScoreField("Full House", 4, 2));
        put("Chance", new ScoreField("Chance", 5, 2));
        put("Yahtzee", new ScoreField("Yahtzee", 6, 2));
        put("Lower Total", new ScoreField("Lower Total", 7, 2));
    }};


    /**
     * Constructor
     *
     * @param title The display name of the panel, passed to YPanel
     */
    public ScoreCardPanel(String title) {
        super(title);
        setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.BOTH;
        gbc.weightx = gbc.weighty = 1;
        for (Map.Entry<String, ScoreField> entry : scoreFields.entrySet()) {
            ScoreField scoreField = entry.getValue();
            gbc.gridy = scoreField.getRow();
            gbc.gridx = scoreField.getButtonColumn();
            add(scoreField.getButton(), gbc);
            gbc.gridx = scoreField.getTextFieldColumn();
            add(scoreField.getTextField(), gbc);
            if (entry.getKey().matches("Bonus|Lower Total|Upper Total")) {
                entry.getValue().getButton().setEnabled(false);
            }
        }
    }

    /**
     * Resets the panel to it's desired state
     */
    public static void reset() {
        for (Map.Entry<String, ScoreField> entry : scoreFields.entrySet()) {
            ScoreField scoreField = entry.getValue();
            scoreField.getButton().setEnabled(true);
            scoreField.getTextField().setText(null);
            if (entry.getKey().matches("Bonus|Lower Total|Upper Total")) {
                entry.getValue().getButton().setEnabled(false);
            }
        }
    }
}
