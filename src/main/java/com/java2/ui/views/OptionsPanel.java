package com.java2.ui.views;

import com.java2.controllers.OptionsListener;
import com.java2.ui.components.YPanel;

import javax.swing.*;
import java.awt.*;

/**
 * Defines the Options Panel containing all options in the game
 *
 * @author Cyrill Näf
 */
public class OptionsPanel extends YPanel {
    private JButton button;

    /**
     * Constructor
     *
     * @param title The display name of the panel, passed to YPanel
     */
    public OptionsPanel(String title) {
        super(title);
        setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.BOTH;
        gbc.weightx = gbc.weighty = 1;
        gbc.gridy = gbc.gridx = 0;
        button = new JButton("New Game");
        button.addActionListener(new OptionsListener());
        add(button, gbc);
    }
}
