package com.java2.ui.views;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

/**
 * Main window view class
 *
 * @author Cyrill Näf
 */
public class MainWindow extends JFrame {
    JPanel rootPanel = new JPanel();

    /**
     * Constructor
     */
    public MainWindow() {
        /**
         * Window settings
         */
        JFrame.setDefaultLookAndFeelDecorated(true);
        setTitle("Yahtzee");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(true);
        setSize(500, 500);
        // set min size avoiding ugly rendering
        setMinimumSize(new Dimension(500,500));
        setLocation(800, 200);


        /**
         * Root Components inizialisation
         */
        rootPanel.setLayout(new BoxLayout(rootPanel, BoxLayout.Y_AXIS));
        rootPanel.add(new ScorePanel("Score"));
        rootPanel.add(new ScoreCardPanel("Score Card"));
        rootPanel.add(new DicePanel("Roll the Dice"));
        rootPanel.add(new OptionsPanel("Options"));
        setContentPane(rootPanel);

        /**
         * Sets Icon of MainWindow
         */
        try {
            setIconImage(ImageIO.read(new File("./src/main/resources/logo.png")));
        } catch (IOException e) {
            e.printStackTrace();
        }

        /**
         * Sets View visible
         */
        setVisible(true);
    }
}
