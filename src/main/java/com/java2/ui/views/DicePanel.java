package com.java2.ui.views;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.io.File;

import javax.swing.*;

import com.java2.controllers.RollTheDiceListener;
import com.java2.models.YahtzeeGame;
import com.java2.ui.components.YPanel;

/**
 * It is the panel class containing the dices and the roll button,
 *
 * @author Cyrill Näf
 */
public class DicePanel extends YPanel {
    private static final String PNG_SUFFIX = ".png";
    private static final String RESOURCES_DIR = "src" + File.separator + "main" + File.separator + "resources" + File.separator + "dices";
    private static final String WHITE_DICE_IMAGE_FILE_NAME = "dicewhite";
    private static final String BLUE_DICE_IMAGE_FILE_NAME = "diceblue";
    private static final String DICE_INITIAL_IMAGE_FILE_NAME = "dicewhite0";

    private static JButton rollButton;
    private static JToggleButton[] diceButton;

    /**
     * Constructor
     *
     * @param title Used to set the title of YPanel (forwarded to parent constructor)
     */
    public DicePanel(String title) {
        super(title);

        setLayout(new FlowLayout(FlowLayout.LEADING));
        setSize(new Dimension(getWidth(), 80));
        // initialize the dices
        createDiceButtons();
        // initialize the roll button
        rollButton = new JButton("Roll");
        rollButton.setEnabled(true);
        // register the listener
        rollButton.addActionListener(new RollTheDiceListener());
        add(rollButton);
    }

    /**
     * Getter for the roll button
     *
     * @return The roll button
     */
    public static JButton getRollButton() {
        return rollButton;
    }

    /**
     * Creation of the dice buttons
     */
    private void createDiceButtons() {
        // create the dice toggle buttons
        diceButton = new JToggleButton[YahtzeeGame.NUMBER_OF_DICES];
        for (int i = 0; i < diceButton.length; i++) {
            diceButton[i] = new JToggleButton();
            diceButton[i].setActionCommand(String.valueOf(i)); // 1...5
            diceButton[i].setPreferredSize(new Dimension(64, 64));
            diceButton[i].addActionListener(new RollTheDiceListener());
            // Disable button. Before the roll button was first time pressed the
            // dices are disabled
            diceButton[i].setEnabled(false);
            // create the initial image of the dice toggle buttons
            ImageIcon initialIcon = new ImageIcon(
                    RESOURCES_DIR + File.separator + DICE_INITIAL_IMAGE_FILE_NAME + PNG_SUFFIX);
            diceButton[i].setIcon(initialIcon);
            add(diceButton[i]);
        }
    }

    /**
     * Sets the state of the dice buttons back to the initial state and icon
     */
    public static void setInitialButtonIcon() {
        for (JToggleButton jToggleButton : diceButton) {
            // Disable button. Before the roll button was first time pressed the
            // dices are disabled
            jToggleButton.setSelected(false);
            jToggleButton.setEnabled(false);
            // create the initial image of the dice toggle buttons
            ImageIcon initialIcon = new ImageIcon(
                    RESOURCES_DIR + File.separator + DICE_INITIAL_IMAGE_FILE_NAME + PNG_SUFFIX);
            jToggleButton.setIcon(initialIcon);
        }
    }

    /**
     * The method is called by pressing the roll button. Enables all the dice
     * buttons if not done already. Loads the image according to the result in
     * the diceResult.
     *
     * @param diceResult The array contains the number of eyes the dices show
     */
    public static void setDiceImages(int[] diceResult) {
        for (int i = 0; i < diceResult.length; i++) {
            diceButton[i].setEnabled(true);
            // sets the image for the button if not selected (white image)
            diceButton[i].setIcon(new ImageIcon(
                    RESOURCES_DIR + File.separator + WHITE_DICE_IMAGE_FILE_NAME + diceResult[i] + PNG_SUFFIX));
            // sets the image for the button if selected (blue image)
            diceButton[i].setSelectedIcon(new ImageIcon(
                    RESOURCES_DIR + File.separator + BLUE_DICE_IMAGE_FILE_NAME + diceResult[i] + PNG_SUFFIX));
        }
    }

    /**
     * Resets the panels state to its initial/desired state
     */
    public static void reset() {
        rollButton.setEnabled(true);
        setInitialButtonIcon();
    }
}
