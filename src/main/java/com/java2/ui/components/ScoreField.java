package com.java2.ui.components;

import com.java2.controllers.ScoreBoardListener;

import javax.swing.*;
import java.awt.*;

/**
 * Used to bundle the JButton and JTextField from the UI together (One Score Entry)
 *
 * @author Cyrill Näf
 */
public class ScoreField {
    private JButton button;
    private JTextField textField;
    private int column, row;

    /**
     * Constructor
     *
     * @param name   Name of the button
     * @param row    Row in the GridBagLayout
     * @param column Column in the GirdBagLayout
     */
    public ScoreField(String name, int row, int column) {
        this.row = row;
        this.column = column;
        button = new JButton(name);
        textField = new JTextField();
        textField.setEnabled(false);
        textField.setMinimumSize(new Dimension(80, 32));

        button.addActionListener(new ScoreBoardListener());
        button.setMinimumSize(new Dimension(80, 32));
    }

    /**
     * The button
     *
     * @return Returns the button instance
     */
    public JButton getButton() {
        return button;
    }

    /**
     * The TextField
     *
     * @return Returns the TextField
     */
    public JTextField getTextField() {
        return textField;
    }

    /**
     * Returns the set row
     *
     * @return GridBagLayout row
     */
    public int getRow() {
        return row;
    }

    /**
     * Calculates and returns the TextFields column based on the provided one
     *
     * @return GridBagLayout Column
     */
    public int getTextFieldColumn() {
        return column + 1;
    }

    /**
     * Calculates and returns the Buttons column based on the provided one
     *
     * @return GridBagLayout Column
     */
    public int getButtonColumn() {
        return column;
    }
}
