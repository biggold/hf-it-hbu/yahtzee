package com.java2.ui.components;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

/**
 * YPanel is the component to instantiate our Yahtzee panels
 *
 * @author Cyrill Näf
 */
public class YPanel extends JPanel {
    /**
     * Constructor
     *
     * @param title The title of the panel (top left corner)
     */
    public YPanel(String title) {
        // create a border and title around the panel
        Border border = BorderFactory.createEtchedBorder(EtchedBorder.LOWERED);
        TitledBorder titleBorder = BorderFactory.createTitledBorder(border, title);
        titleBorder.setTitleJustification(TitledBorder.LEFT);
        setBorder(titleBorder);
    }
}
